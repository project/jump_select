<?php 
/**
 * @Param string $view_name : "Views" name
 * @Param string $view 		: "Views" list
 * @Param string $template  : name template
 */
 ?>
<div class="quick-select">
  <div class="select-area qJSArea" id="<?php print $view_name; ?>Area">
	<div class="select-area-left">&nbsp;</div>
	<div class="select-area-right">&nbsp;</div>
	<div id="name_view" class="<?php print $view_name; ?>"></div>
		<div class="select-area-center qJSText" id="<?php print $view_name; ?>Text"><?php print t('Select');?></div>
		<ul class="select-options-invisible qJSOptions" id="<?php print $view_name; ?>Options">
		  <?php print $view; ?>
		</ul>
	</div>
</div> 