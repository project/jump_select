-- SUMMARY --

This module provides a box in the options list, generated module views on the means to insert the text tag [jumpselect]view_name[/jumpselect].
For example, you can use this module to display more items in the list.
Demo - http://

-- REQUIREMENTS --

views

-- INSTALLATION --

* Tick module on page modules

-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >> Jump Select module:

* Customize the menu settings in Administer >> Site configuration >> Settings for jump select

* Tick settings "Do not show hover links over views" on page admin/build/views/tools for better usability
 
-- USE --

1. Set list in the module to form views default with paramters "Style" - HTML List and "Defaults: Style options" - Unordered list.
2. Create your own filter settings which set the processing jumpselect.
3. Insert the text [jumpselect]view_name[\jumpselect] and check the filter you created.
4. Customize style using jumpSelect.css and jump_select.tpl.php

-- CONTACT --

Developer http://drupal.org/user/203339
This project has been sponsored by:
* JASMiND
  Is an independent professional consulting group with extensive background in telecommunications, IT, software development and implementation. Visit http://jmproduct.ru/en for more information.
* AEROFLOT
  Largest air carrier in Russia. Visit http://aeroflot.ru for more information.